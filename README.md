# Sankhya: All Bangla Open-source Dataset

These datasets are publicly available Bangla handwritten datasets. All of those were available in different image and folder structure format. We converted those in .csv format to easy use and make them all in one universal format.
These steps will help all the researchers to worry less about the dataset preprocessing and other data-related stuff. Also, help them not to worry about dealing with different formats of data as we make them in one universal format.

## Importing Necessary Library
```
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

%matplotlib inline
np.random.seed(2)
sns.set(style='white', context='notebook', palette='deep')
```

## Load data
To use a single dataset:
```
train = pd.read_csv("bdrw.csv")
```
To marge multiple dataset:
```
train = pd.read_csv("ekush_maleDigits.csv")
train2 = pd.read_csv("ekush_femaleDigits.csv")
frames = [train, train2]
train = pd.concat(frams, ignore_index=True)
del  train2
```

## Separating label and image
```
Y_train = train["label"]

# Drop 'label' column
X_train = train.drop(labels = ["label"],axis = 1) 

# free some space
del train 

g = sns.countplot(Y_train)

Y_train.value_counts()
```

Now our data is ready for training. But we will do some pre-processing to boost the efficiency of the algorithm. 

## Check for null and missing values
`X_train.isnull().any().describe()`

## Normalization
We perform a grayscale normalization to reduce the effect of illumination's differences.
Moreover, the CNN converge faster on [0..1] data than on [0..255].

`X_train = X_train / 255.0`
## Reshape
Train and test images (28px x 28px) has been stock into pandas.Dataframe as 1D vectors of 784 values. We reshape all data to 28x28x1 3D matrices.

Tensorflow.Keras requires an extra dimension in the end which correspond to channels. These datasets images are gray scaled so it use only one channel. For RGB images, there is 3 channels, we would have reshaped 784px vectors to 28x28x3 3D matrices.
```
# Reshape image in 3 dimensions (height = 28px, width = 28px , channel = 1)
X_train = X_train.values.reshape(-1,28,28,1)
```
## Label encoding
Labels are 10 digits numbers from 0 to 9. We need to encode these lables to one hot vectors (ex : 2 -> [0,0,1,0,0,0,0,0,0,0]).
```
# Encode labels to one hot vectors (ex : 2 -> [0,0,1,0,0,0,0,0,0,0])
Y_train = to_categorical(Y_train, num_classes = 10)
```

## Split training and valdiation set
```
# Set the random seed
random_seed = 2
# Split the train and the validation set for the fitting
X_train, X_val, Y_train, Y_val = train_test_split(X_train, Y_train, test_size = 0.2, random_state=random_seed)
```

We chose to split the train set into two parts: a small fraction (20%) became the validation set in which the model is evaluated and the rest (90%) is used to train the model.

Since we have lots of training images of balanced labels (see Separating label and image), a random split of the train set doesn't cause some labels to be over-represented in the validation set. Be careful with some unbalanced dataset a simple random split could cause inaccurate evaluation during the validation.
To avoid that, you could use stratify = True option in train_test_split function (Only for >=0.17 sklearn versions).

We can get a better sense of one of these examples by visualizing the image and looking at the label.
```
# Some examples
g = plt.imshow(X_train[0][:,:,0])
```

#  Sankhya Model
## Importing Necessary Library
```
from tensorflow.keras.models import load_model
```
## Load Model
```
model = load_model('model.h5')
# summarize model.
model.summary()
```

## Train Model
```
history = model.fit_generator(datagen.flow(X_train,Y_train, batch_size=batch_size),
                              epochs = epochs, validation_data = (X_val,Y_val),
                              verbose = 2, steps_per_epoch=X_train.shape[0] // batch_size)
```

# Special Thanks to
* **[Ekush Dataset](https://shahariarrabby.github.io/ekush)**
* **[NumtaDB Dataset](https://github.com/BengaliAI/Numta)**
* **[cmaterdb Dataset](https://code.google.com/archive/p/cmaterdb)**
* **[BDRW dataset](https://www.kaggle.com/debdoot/bdrw)**


<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a>
